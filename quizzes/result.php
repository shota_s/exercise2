<?php 
session_start(); 
require('../dbconnect.php');

// カテゴリ名を取得
$cate3 = $_SESSION['cate_id'];

$categories = $db->prepare('SELECT question_name,count(DISTINCT question) FROM categories LEFT JOIN quizzes on categories.id = quizzes.category_id WHERE categories.id = :cate');
$categories->bindParam(':cate',$cate3);
$categories->execute();
$cate = ($categories->fetchAll());
 // 正誤判定
 $ans = $_POST['kotae'];
 $correct = $_POST['correct'];

 if($ans == $correct){
   $hantei="正解!";
   if(isset($_SESSION['ok_count'])){
     $ok_count=$_SESSION['ok_count']+1;
   }else{
     $ok_count=1;
   }
   $_SESSION['ok_count']=$ok_count;
 }else{
   $hantei="不正解!";
   if(!isset($_SESSION['ok_count'])){
     $ok_count=0;
   }else{  
     $ok_count=$_SESSION['ok_count'];
   }
 }

 // dbに保存
$ques_db = $_SESSION['qcode']; 

$score = $db->prepare('SELECT max(id) FROM users');
$score->execute();
$ques_name = $score->fetch();

if($ans == $correct){
 $result = 1;
}else{
 $result = 0; 
}

$score2 = $db->prepare('INSERT INTO scores(user_id, quiz_id, answer, is_correct, result) VALUES(?,?,?,?,?)');
$score2->execute(array(
 $ques_name[0],
 $ques_db, 
 $ans,
 $correct,
 $result
));

?>
<!doctype html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../css/quizzes/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous"> 
    <title>Exercise2</title>
  </head>
  <body>
    <main>
      <div class="container">
        <h4 class="text-center mt-5"><?php echo $cate[0]['question_name'] ?></h4>
          <div class="text-center">(全<?php echo $cate[0]['count(DISTINCT question)'] ?>問)</div>
            <h4 class="text-center mt-5">
              <?php echo $_SESSION['name'];?> さんの点数は<?php echo $_SESSION['ok_count'];?>点です
            </h4>
            <div class="col text-center">
              <a class="btn btn-secondary mt-4" href="../categories/index.php" role="button">一覧へ戻る</a>
              <?php session_destroy();?>   
            </div>  
      </div>
    </main>
  </body>    
</html>