<?php 
  session_start();
  require('../dbconnect.php');

  // 問題別スコアを取得
  $_SESSION['cate_id'] = $_REQUEST['id'];
  $cate = $_SESSION['cate_id'];

  $categories_score = $db->prepare("SELECT users.name, sum(scores.result), users.created_at FROM scores LEFT JOIN users on scores.user_id = users.id LEFT JOIN quizzes on scores.quiz_id = quizzes.id WHERE quizzes.category_id = :cate GROUP BY users.name ORDER BY users.created_at");
  $categories_score->bindParam(':cate',$cate);
  $categories_score->execute();
  $cate_scores = ($categories_score->fetchAll());
  
  $cate_score = array_column($cate_scores ,NULL , 'name');

  // カテゴリ名を取得
  $cate3 = $_SESSION['cate_id'];

  $categories = $db->prepare('SELECT question_name FROM categories WHERE id = :cate');
  $categories->bindParam(':cate',$cate3);
  $categories->execute();
  $cate = ($categories->fetchAll());
  
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" href="../css/categories/_index.css" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Exercise2</title>
  </head>
  <body>
  <h2 class="center-block cate-title mt-5">「<?php echo $cate[0]['question_name'] ?>」の回答者一覧(<?php echo count($cate_scores); ?>人)</h2>
  <div class="col text-center">
      <a class="btn btn-secondary mt-4" href="../categories/index.php" role="button">一覧へ戻る</a>
  </div>  
  <br>
    <div class="container"> 
        <table class="table main-table">
          <thead class="thead-light">
            <tr>
              <th scope="col"class="text-center">回答者名</th>
              <th scope="col"class="text-center">スコア</th>
              <th scope="col"class="text-center">回答日時</th>
              <th scope="col"class="text-center"></th>
            </tr>
          </thead>
          <tbody>
          <?php foreach ($cate_score as $c_score) : ?>
            <tr>
              <th scope="row" class="text-center"><?php echo $c_score['name'] ;?></th>
              <td class="text-center"><?php echo $c_score['sum(scores.result)'] ;?></td>
              <td class="text-center"><?php echo $c_score['created_at'] ;?></td>
              <td class="text-center">
                <a class="btn-sm btn btn-secondary" href="#" role="button">回答詳細</a>
              </td>
            </tr> 
          <?php endforeach; ?>
          </tbody>
        </table>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
  </body>
</html>